lensingGW - a Python package for lensing of gravitational waves
===============================================================

Copyright © 2020 Giulia Pagano

``lensingGW`` is a software package to simulate lensed gravitational waves in ground-based interferometers
from arbitrary compact binaries and lens models.

Its algorithm allows to resolve strongly lensed images and microimages simultaneously,
such as the images resulting from hundreds of microlenses embedded in galaxies and galaxy clusters.

----

* `Source code <https://gitlab.com/gpagano/lensinggw>`_
* `Installation and requirements <https://gpagano.gitlab.io/lensinggw/installation.html>`_
* `Documentation and worked-out examples <https://gpagano.gitlab.io/lensinggw/>`_
* ``lensingGW`` is available under the `GNU General Public License v3 or later (GPLv3+) <https://gitlab.com/gpagano/lensinggw/-/blob/master/LICENSE>`_

* The design philosophy of ``lensingGW`` is presented in `Pagano et al. 2020 <https://arxiv.org/abs/2006.12879>`_.
  Please cite this paper and link to https://gitlab.com/gpagano/lensinggw when you use ``lensingGW`` in a publication
* ``lensingGW`` is based on `Lenstronomy <https://github.com/sibirrer/lenstronomy>`_ (`Birrer & Amara 2018 <https://ui.adsabs.harvard.edu/abs/2018PDU....22..189B/abstract>`_), as described in the `package documentation <https://gpagano.gitlab.io/lensinggw/lensinggw.html>`_. Please also cite Lenstronomy as suggested on https://github.com/sibirrer/lenstronomy if you use ``lensingGW`` in a publication


Please submit bugs/suggestions etc to giulia.pagano@pi.infn.it.