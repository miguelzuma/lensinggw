Only macromodel
===============
* `Example 4 - Solve the lens model (only macromodel) <https://gitlab.com/gpagano/lensinggw/-/blob/master/lensinggw/examples/binary_point_mass_only_macromodel.py>`__

If the lens model is composed of a macromodel and a micromodel (such as multiple microlenses), users can require the study of the macromodel only
by enabling an appropriate flag in the solver settings

.. code-block:: python

        solver_settings.update({'OnlyMacro': True})
        
| This functionality, combined with the multi-component choice, allows to define the lens model once and for all and then
  investigate the impact of each component on the resulting images by acting on the solver settings.
| For example, one can 

  + enable *OnlyMacro* and change the lens profile/profile group indicated as macromodel - assesses the impact of the macromodel components on the images of the macromodel
  + disable *OnlyMacro* and change the lens profile/profile group indicated as macromodel - assesses the impact of the macromodel components on the images of the complete model
