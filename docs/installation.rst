Installation & requirements
---------------------------

- ``lensingGW`` requires a `forked version of Lenstronomy <https://github.com/gipagano/lenstronomy>`_. The following commands download it in the current folder and install it

  .. code-block:: bash
  
      $ git clone https://github.com/gipagano/lenstronomy.git
      $ cd lenstronomy
      $ python setup.py install 

- ``lensingGW`` can then be installed as follows:

  .. code-block:: bash
  
      $ cd ..
      $ git clone https://gitlab.com/gpagano/lensinggw.git
      $ cd lensinggw
      $ python setup.py install
    
- It requires ``python>=3.7``

