.. lensingGW documentation master file, created by
   sphinx-quickstart on Thu Apr 23 11:37:54 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================================
lensingGW - a Python package for lensing of gravitational waves
===============================================================

Copyright © 2020 Giulia Pagano

``lensingGW`` is a software package to simulate lensed gravitational waves in ground-based interferometers
from arbitrary compact binaries and lens models.

Its algorithm allows to resolve strongly lensed images and microimages simultaneously,
such as the images resulting from hundreds of microlenses embedded in galaxies and galaxy clusters.

Please submit bugs/suggestions etc to giulia.pagano@pi.infn.it.

----   
   
.. toctree::
   :maxdepth: 1
   :caption: Contents:
   
   installation
   usage   
   lensinggw
   notes
   acknowledgment
   
.. include:: installation.rst
.. include:: usage.rst
.. include:: notes.rst
.. include:: acknowledgment.rst

==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
