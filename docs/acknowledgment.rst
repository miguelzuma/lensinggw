Attribution and source code
---------------------------

* `Source code <https://gitlab.com/gpagano/lensinggw>`_
* ``lensingGW`` is available under the `GNU General Public License v3 or later (GPLv3+) <https://gitlab.com/gpagano/lensinggw/-/blob/master/LICENSE>`_

* The design philosophy of ``lensingGW`` is presented in `Pagano et al. 2020 <https://arxiv.org/abs/2006.12879>`_.
  Please cite this paper and link to https://gitlab.com/gpagano/lensinggw when you use ``lensingGW`` in a publication
* ``lensingGW`` is based on `Lenstronomy <https://github.com/sibirrer/lenstronomy>`_ (`Birrer & Amara 2018 <https://ui.adsabs.harvard.edu/abs/2018PDU....22..189B/abstract>`_), as described in the `package documentation <https://gpagano.gitlab.io/lensinggw/lensinggw.html>`_. Please also cite Lenstronomy as suggested on https://github.com/sibirrer/lenstronomy if you use ``lensingGW`` in a publication



