import setuptools 
import os
from codecs import open

def readme():
    with open('README.rst') as f:
        return f.read()
        
def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(here, rel_path), 'r') as fp:
        return fp.read()

def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith('__version__'):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")

setuptools.setup(name                          = "lensinggw",
                 version                       = get_version("lensinggw/__init__.py"),
                 description                   = 'lensingGW: Python package for gravitational wave lensing',
                 long_description_content_type = "text/markdown",
                 long_description              = readme(),
                 keywords                      = 'gravitational wave lensing',
                 author                        = 'Giulia Pagano',
                 author_email                  = 'giulia.pagano@pi.infn.it',
                 url                           = 'https://gitlab.com/gpagano/lensinggw',
                 license                       = 'GNU General Public License v3 or later (GPLv3+)',
                 packages                      = setuptools.find_packages(),
                 python_requires               = '>=3.7',
                 include_package_data          = True,
                 install_requires              = ['numpy>=1.18.1', 'scipy>=1.4.1', 'astropy>=4.0', 'matplotlib>=3.1.3 ', 'lalsuite>=6.70',                                 'mpmath'], 
                 classifiers                   = ['Development Status :: 5 - Production/Stable',
                                                  'Intended Audience :: Science/Research',
                                                  'Natural Language :: English',
                                                  'License :: OSI Approved :: GNU General Public License v3 or later '
                                                  '(GPLv3+)',
                                                  'Programming Language :: Python :: 3.7',
                                                  'Topic :: Lensing :: Physics'])